#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>

typedef struct TSoldier
 {
   struct TSoldier   * m_Next;
   int                 m_PersonalID;
   char                m_SecretRecord[64];
 } TSOLDIER;
 
#endif /* __PROGTEST__ */
 
void			addOne					    ( TSOLDIER	     ** p,
										TSOLDIER     *** temp )
 {
	 if ( *p ){
		 **temp = *p;
		 *temp = &(*p) -> m_Next;
		 *p = **temp;
	 }
 }

TSOLDIER         * mergePlatoons                           ( TSOLDIER        * p1,
									      TSOLDIER        * p2 )
 {
	 
	TSOLDIER * outValue = NULL;
	TSOLDIER ** temp = &outValue; /*  predavam adresu vysledku ako navratovej hodnoty,  ak sa neskor meni temp, meni sa aj outValue*/
	 
	 while ( p1 || p2 ){
		 
		 /*
		 if ( p1 ){
			*temp = p1;
			temp = &p1 -> m_Next;
			p1 = *temp; 
		 // pridavam cely spojak c1 ; nasledne pridavam spojak c2. sucasne "prepisujem" vysledny 
		 }
		 */
		 addOne( &p1, &temp );
		 /*
		if ( p2 ){
			*temp = p2;
			temp = &p2 -> m_Next;
			p2 = *temp; 
		 }
		 */
		 addOne( &p2, &temp );
		 
	 }
	 
	 *temp = NULL;
	 
	 return outValue;
	 
	 /* rekurzivne, zjavne zle.... :'(
	 
    if (p1 == NULL)
        return p2;
    if (p2 == NULL)
        return p1;
    TSOLDIER *tmp1 = mergePlatoons(p1-> m_Next, p2-> m_Next);
    TSOLDIER *tmp2 = p1;
    tmp2-> m_Next = p2;
    p2-> m_Next = tmp1;
    return tmp2;
	 */
 }
 
void			 getHalf					( int 		size,
									TSOLDIER	*** out,
									TSOLDIER	** temp )
 {
	 int i;
	 
	 for ( i = 0 ; i < size ; i++ ){
		**out = *temp; 
		*out = &(*temp) -> m_Next; 
		*temp = **out;
	}
 }

 
void               splitPlatoon                            ( TSOLDIER        * src, 
								      TSOLDIER      ** p1,
								      TSOLDIER      ** p2 )
 {
	TSOLDIER * temp;
	TSOLDIER ** outValue; /* neskor cez nu predavam do p1 & p2 */
	 
	 /* !!!!!!!!!!!!!!!! dolezite !!!!!! tym ze sa mi zmeni ptr*/
	
	int size = 0;
	
	for ( temp = src ; temp != NULL ; temp = temp -> m_Next ){
		size++;
	}
	
	temp = src;
	*p1 = NULL;
	*p2 = NULL;
	outValue = p1;
	
	getHalf( (size/2), &outValue, &temp );
	
	/*
	for ( i = 0 ; i < ( size / 2 ) ; i++ ){
		*outValue = temp; //priradujem do struktury hlavnej
		outValue = &temp -> m_Next; // outValue je ** ; musis "vyvazit" 
		temp = *outValue; // priradujem nadchadzajucu adresu, aby som sa mohol posunut 
	}
	*/
	
	
	*outValue = NULL; /* "Uzemnenie" ; chod spat
	teraz idem robit to iste pre druhu polku */
	outValue = p2; 
	
	
	getHalf( (size/2), &outValue, &temp );
	
	/*
	for ( i = 0 ; i < ( size / 2 ) ; i++ ){
		*outValue = temp; //priradujem do struktury hlavnej
		outValue = &temp -> m_Next; // outValue je ** ; musis "vyvazit" 
		temp = *outValue; // priradujem nadchadzajucu adresu, aby som sa mohol posunut
	}
	*/
	
	*outValue = NULL;
	
	free(temp); 
 }
 
void               destroyPlatoon                          ( TSOLDIER        * src )
 {
	TSOLDIER  * tmp;
	 
	while ( src ){
		tmp = src -> m_Next;   /* ulozit kopiu ... */
		free  ( src );
		src = tmp;           /* ... abychom z uvolnene pameti jiz necetli */
	}
	//src -> m_Next = NULL;
	//free ( src );
 }
  
#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
 {
	 
	 
 //  TSOLDIER  * a, * b, * c;

/* list a: 0 -> 1 -> 2 -> 3 -> 4 */
/* list b: 10 -> 11 -> 12 -> 13 -> 14 */
//c = mergePlatoons ( a, b );
/* list c: 0 -> 10 -> 1 -> 11 -> 2 -> 12 -> 3 -> 13 -> 4 -> 14 */
//splitPlatoon ( c, &a, &b );
/* list a: 0 -> 10 -> 1 -> 11 -> 2 */
/* list b: 12 -> 3 -> 13 -> 4 -> 14 */
//destroyPlatoon ( a );
//destroyPlatoon ( b );

/* list a: 0 -> 1 -> 2 */
/* list b: 10 -> 11 -> 12 -> 13 -> 14 */
//c = mergePlatoons ( a, b );
/* list c: 0 -> 10 -> 1 -> 11 -> 2 -> 12 -> 13 -> 14 */
//splitPlatoon ( c, &a, &b );
/* list a: 0 -> 10 -> 1 -> 11 */
/* list b: 2 -> 12 -> 13 -> 14 */
//destroyPlatoon ( a );
//destroyPlatoon ( b );

/* list a: 0 -> 1 -> 2 */
/* list b: 10 -> 11 -> 12 -> 13 */
//c = mergePlatoons ( a, b );
/* list c: 0 -> 10 -> 1 -> 11 -> 2 -> 12 -> 13 */
//splitPlatoon ( c, &a, &b );
/* list a: 0 -> 10 -> 1 */
/* list b: 11 -> 2 -> 12 */
//destroyPlatoon ( a );
//destroyPlatoon ( b );

 }
#endif /* __PROGTEST__ */
