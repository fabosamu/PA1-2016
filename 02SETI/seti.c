#include<stdio.h>
#include<stdlib.h>

int GCD(int a, int b){
int tmp = a;

	if(a<b){
		tmp = a;
		a = b;
		b = tmp;
	}
	while(b>0){
		tmp = a % b;
		a = b;
		b = tmp;
	}
return a;
}

int main( int argc, char * argv [] ){
	char c;
	int enter=0, pipe=0, poc1=0, poc2=0;
	long long int pred1=0, po1=0, cele1=0,
	pred2=0, po2=0, cele2=0;
	
	while ( !feof (stdin)){
		scanf("%c",  &c);
		int j = c-97;
		if ( c == '\n' ){
			if (enter < 1){
				enter++;
				pred1 = pred2;
				po1 = po2;
				cele1 = pred1+po1;
				pred2 = 0;
				po2 = 0;
			}
			cele2 = pred2+po2;
		}else if ( c == '|' ){
			pipe++;
			pred2 = po2;
			po2 = 0;
		} else if ( c>='a' && c <='z' ){
			int i = 1 << j;
			po2 += i;
		} else {
			printf("Zpravy:\n");
			printf("Nespravny vstup.\n");
			return 1;
		}
	}
printf("Zpravy:\n");
	if ( pipe != 2 && enter != 2 ){
		printf("Nespravny vstup.\n");
		return 1;
	}	
	if (pred1 == 0)
			poc1++;
	if (pred2 == 0)
			poc2++;
	if (po1 == 0)
			poc1++;
	if (po2 == 0)
			poc2++;
	
	if (poc2 == 2 || poc1 ==2){
		printf("Nespravny vstup.\n");
		return 1;
	} else if (poc1 == 1 && poc2 == 1) {
		printf("Synchronizace za: 0\n");
		return 0;
	} else if ( labs(pred1-pred2) % GCD(cele1,cele2) != 0 ){
		printf("Nelze dosahnout.\n");
		return 1;
	} else {
		/*printf("%d mod %d\n", abs(pred1-pred2), GCD(cele1,cele2) );*/
		long long int x=pred1, y=pred2;
		while (1){
			if (x==y)
				break;
			if (x<y)
				x += cele1;
			else
				y += cele2;
		}
		printf("Synchronizace za: %lld\n", x);
		return 0;
	}

}







