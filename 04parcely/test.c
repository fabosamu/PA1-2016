#include<stdio.h>
#include<string.h>


/*-------------------NACITAJ MAPU----------------*/
int GetMap( int w, int h, int array[][2000]){
	int tmp;
	for ( int i = 0; i < h; i++ ){
		for ( int j = 0; j < w; j++ ){
			if ( scanf("%d", &tmp ) != 1 ){
				return 0;
			}
			array[i][j] = tmp;
		}
	}
	/*for ( int i = 0; i < h; i++ ){
		for ( int j = 0; j < w; j++ ){
			printf("%d ", array[i][j] );
		}
		printf("\n");
	}*/
	return 1;
}

void EmptyTmpArray(int tmp[2000], int h){
	int i;
	for ( i = 0 ; i < h ; i++ )
		tmp[i]=0;
}

/*----------------FUNKCIA SEARCH-----------------*/
int Count( int w, int h, int * x, int a[][2000], int swich){
	int i, j, k, l, sum=0,cnt = 0;
	static int tmp[2000];
	
	EmptyTmpArray(tmp, h);
	for ( l = 0 ; l < w ; l++ ){
		for ( k = l ; k < w ; k++ ){
			for ( j = 0 ; j < h ; j++ ){
				for  ( i = j ; i < h ; i++ ){
					if ( j == 0 )
						tmp[i] += a[i][k];
					sum += tmp[i];
					if ( sum == *x ){
						//printf("i%d j%d k%d l%d aIK%d tmpI%d \n", i,j,k,l, a[i][k], tmp[i]);
						cnt++;
						if ( swich )
							printf("%d @ (%d,%d) - (%d,%d)\n", *x, l, j, k, i);
					}
				}
				sum = 0;
			}
		}
		EmptyTmpArray(tmp, h);
	}
	return cnt;
}

int main ( int argc, char * argv [] ){
	static int array[2000][2000];
	int w, h, x, res, sw;
	char dem[5];
	
	printf("Velikost mapy:\n");
	if ( scanf("%d %d", &w, &h) != 2 ||
		w < 1 || w > 2000 ||
		h < 1 || h > 2000){
		printf("Nespravny vstup.\n");
		return 0;
	}
	printf("Cenova mapa:\n");	
	if ( ! GetMap( w, h, array) ) {
		printf("Nespravny vstup.\n");
		return 1;
	}
	
	printf("Dotazy:\n");
	while ( 1 ){
		res =  scanf("%5s %d", dem, &x);
		if ( res == EOF )
			break;
		else if (res != 2){
			printf("Nespravny vstup.\n");
			return 1;
		} else if ( strcmp (dem, "count") == 0 ){
			sw = 0;
			printf ( "Celkem: %d\n", Count(w, h, &x, array, sw) );
		} else if ( strcmp (dem, "list") == 0 ){
			sw = 1;
			printf ( "Celkem: %d\n", Count(w, h, &x, array, sw) );
		} else {
			printf("Nespravny vstup.\n");
			return 1;
		}
	}//este pories ci to nevyhadzuje debilne nespravne vystupy.
	
	return 0;
}
