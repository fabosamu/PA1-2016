#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#endif /* __PROGTEST__ */

/*------------------------JE ROK PRESTUPNY?----------------------*/
int IsLeapYear (int year)
{
	/*roky nejsou p?estupn�,
s v�jimkou let d?liteln�ch 4, kter� jsou p?estupn�,
s v�jimkou let d?liteln�ch 100, kter� nejsou p?estupn�,
s v�jimkou let d?liteln�ch 400, kter� jsou p?estupn�,
s v�jimkou let d?liteln�ch 4000, kter� nejsou p?estupn�.*/
	if ( year % 4000 == 0 )
		return 0;
	if ( year % 400 == 0 )
		return 1;
	if ( year % 100 == 0 )
		return 0;
	if ( year % 4 ==0 )
		return 1;
	return 0;
		
}

/*---------------------KOLKO DNI V MESIACI?--------------------*/
int GetDaysOfMonth (int month, int year)
{
	switch ( month )
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		case 2:
			return 28 + IsLeapYear(year);
	}
	return 0;
}

/*---------------------JE SVIATOK?-------------------------------*/
int IsHoliday ( int m, int d ){
	/* 1.1,  1.5,  8.5,  5.7,  6.7, 28.9,  28.10,  17.11,  24.12, 25.12 26.12*/
	if ( ( d == 1 && m == 1 ) ||
		( d == 1 && m == 5 ) ||
		( d == 8 && m == 5 ) ||
		( d == 5 && m == 7 ) ||
		( d == 6 && m == 7 ) ||
		( d == 28 && m == 9 ) ||
		( d == 28 && m == 10 ) ||
		( d == 17 && m == 11 ) ||
		( d == 24 && m == 12 ) ||
		( d == 25 && m == 12 ) ||
		( d == 26 && m == 12 ) )
		return 1;
	else
		return 0;
}

/*---------------------GAUSSOVA METODA---------------------*/
int NumberOfDay ( int y, int m, int d ){
	return(d+=m<3?y--:y-2,23*m/9+d+4+y/4-y/100+y/400-y/4000)%7;
}

/*---------------------JE PRACOVNY DEN?---------------------*/
int IsWorkDay ( int y, int m, int d )
{
  /* todo 
	rok je v?t?� roven 2000 
		(v?echna data p?ed rokem 2000 pova?ujeme za neplatn�),
	m?s�c je platn� (1 a? 12),
	den je platn� (1 a? po?et dn� v m?s�ci),
ve funkci CountWorkDays je datum po?�tku intervalu <= datum konce intervalu.*/
	if ( y < 2000 || ( m < 1 && m > 12 ) || 
		d < 1 || d > GetDaysOfMonth(m, y) )
		return 0;
	if ( ! IsHoliday( m, d ) ){
		char tmp;
		tmp = NumberOfDay( y, m, d );
		if ( tmp > 0 && tmp < 6 )
			return 1;
		else
			return 0;
	} else 
		return 0;
}

/*------------------JE SPRAVNY INTERVAL?-------------------*/
int CorrectInterval ( int y1, int m1, int d1, int y2, int m2, int d2) {
	if ( y1 > y2 )
		return 0;
	if ( y1 == y2 ){
		if ( m1 > m2 )
			return 0;
		if ( m1 == m2 )
			if ( d1 > d2 )
				return 0;
			
	}
	if ( y1 < 2000 || ( m1 < 1 && m1 > 12 ) || 
		d1 < 1 || d1 > GetDaysOfMonth(m1, y1) )
		return 0;
	if ( y2 < 2000 || ( m2 < 1 && m2 > 12 ) || 
		d2 < 1 || d2 > GetDaysOfMonth(m2, y2) )
		return 0;
	
	return 1;
}

/*-----------------------POCITAJ PRACOVNE DNI------------------*/
int CountWorkDays ( int y1, int m1, int d1,
                    int y2, int m2, int d2,
                    int * cnt )
{
	*cnt = 0;
	if ( ! CorrectInterval ( y1, m1, d1, y2, m2, d2) ){
		/*printf("zla korekcia %d \n",CorrectInterval ( y1, m1, d1, y2, m2, d2));*/
		return 0;
	}
	if ( IsWorkDay ( y1, m1, d1 ) ){
		*cnt += 1;
		//printf("%d\n",*cnt);
		}
	while ( ! ( y1 == y2 && m1 == m2 && d1 == d2 ) ){
		if ( d1 < GetDaysOfMonth (m1, y1) ){
			d1++;
			/*printf("%d.%d.%d	vs	%d.%d.%d\n",d1,m1,y1,d2,m2,y2);*/
		}
		else {
			d1 = 0;
			if ( m1 < 12 )
				m1++;
			else {
				m1 = 1;
				if ( y1 < y2 )
					y1++;
			}
		}
		if ( IsWorkDay ( y1, m1, d1 ) ){
		*cnt += 1;
		//printf("%d\n",*cnt);
		}
	} 
	printf("%d\n",*cnt);
	return 1;
}


#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
  int cnt;
  assert (!IsLeapYear (2001));
  assert (!IsLeapYear (2005));
  assert (IsLeapYear(2008));
  assert (IsLeapYear(2104));
  assert (!IsLeapYear (2200));
  assert (IsLeapYear(2000));
  assert (IsLeapYear(4400));
  assert (!IsLeapYear (8000));
  assert (!IsLeapYear (2100));
  assert (   IsWorkDay ( 2016, 11, 11 ) );
  assert ( ! IsWorkDay ( 2016, 11, 12 ) );
  assert ( ! IsWorkDay ( 2016, 11, 17 ) );
  assert ( ! IsWorkDay ( 2016, 11, 31 ) );
  assert (   IsWorkDay ( 2016,  2, 29 ) );
  assert ( ! IsWorkDay ( 2004,  2, 29 ) );
  assert ( ! IsWorkDay ( 2001,  2, 29 ) );
  assert ( ! IsWorkDay ( 1996,  1,  1 ) );
  assert ( ! IsWorkDay ( 1996,  5,  1 ) );
  assert ( ! IsWorkDay ( 1996,  5,  8 ) );
  assert ( ! IsWorkDay ( 1996,  7,  5 ) );
  assert ( ! IsWorkDay ( 1996,  7,  6 ) );
  assert ( ! IsWorkDay ( 1996,  9,  28 ) );
  assert ( ! IsWorkDay ( 1996,  10,  28 ) );
  assert ( ! IsWorkDay ( 1996,  11,  17 ) );
  assert ( ! IsWorkDay ( 1996,  12,  24 ) );
  assert ( ! IsWorkDay ( 1996,  12,  25 ) );
  assert ( ! IsWorkDay ( 1996,  12,  26 ) );
  assert (   IsWorkDay ( 2016,  11,  11 ) );
  assert (   IsWorkDay ( 2008,   1,   2 ) );
  assert (   IsWorkDay ( 2016,   2,  29 ) );
  assert ( ! IsWorkDay ( 2016,  11,  12 ) );
  assert ( ! IsWorkDay ( 2016,  11,  17 ) );
  assert ( ! IsWorkDay ( 2016,  11,  31 ) );
  assert ( ! IsWorkDay ( 2004,   2,  29 ) );
  assert ( ! IsWorkDay ( 2001,   2,  29 ) );
  assert ( ! IsWorkDay ( 1996,   1,   1 ) );
  assert ( ! IsWorkDay ( 2018, 123, 124 ) );
  assert ( ! IsWorkDay ( 2100, 2, 29 ) );
  
  
  assert ( CountWorkDays ( 2016, 11,  1, 
					2016, 11, 30, &cnt ) == 1
           && cnt == 21 );
  assert ( CountWorkDays ( 2016, 11,  1,
					2016, 11, 17, &cnt ) == 1
           && cnt == 12 );
  assert ( CountWorkDays ( 2016, 11,  1,
					2016, 11,  1, &cnt ) == 1
           && cnt == 1 );
  assert ( CountWorkDays ( 2016, 11, 17,
					2016, 11, 17, &cnt ) == 1
           && cnt == 0 );
  assert ( CountWorkDays ( 2016,  1,  1,
					2016, 12, 31, &cnt ) == 1
           && cnt == 254 );
  assert ( CountWorkDays ( 2015,  1,  1,
					2015, 12, 31, &cnt ) == 1
           && cnt == 252 );
  assert ( CountWorkDays ( 2000,  1,  1,
					2016, 12, 31, &cnt ) == 1
           && cnt == 4302 );
  assert ( CountWorkDays ( 2001,  2,  3,
					2016,  7, 18, &cnt ) == 1
           && cnt == 3911 );
  assert ( CountWorkDays ( 2014,  3, 27,
					2016, 11, 12, &cnt ) == 1
           && cnt == 666 );
  assert ( CountWorkDays ( 2001,  1,  1,
					2000,  1,  1, &cnt ) == 0 );
  assert ( CountWorkDays ( 2001,  1,  1,
					2015,  2, 29, &cnt ) == 0 ); 
  assert ( CountWorkDays ( 2004,  12, 25,
					2004,  12, 31, &cnt ) == 1 && cnt == 5 );
  assert (!IsWorkDay(2018, 123, 124));
  assert (CountWorkDays(2004, 12, 25, 2004,12,31,&cnt) == 1 && cnt == 5);
  assert (CountWorkDays(1999, 12, 31, 2000, 12, 31, &cnt) == 0 && cnt == 0);
  assert (CountWorkDays ( 2076, 7, 4, 5094763, 6, 3, &cnt ) ==1 && cnt==1288603482);
  assert (CountWorkDays ( 2416, 4, 3, 2531, 1, 4, &cnt ) == 1 && cnt==29038);
  assert (CountWorkDays ( 2008, 9, 30, 2008, 11, 11, &cnt ) == 1 &&cnt ==30);
  assert (CountWorkDays ( 2000, -10, -10, 2200, 10, 1, &cnt ) == 0);
  assert (CountWorkDays ( 2000, 5, 8, 2000, 12, 31, &cnt ) == 1 && cnt == 163);

  assert ( CountWorkDays ( 2000, -10, -10,    2200, 10,  1, &cnt ) == 0 );
  assert ( CountWorkDays ( 1999,  12,  31,    2000, 12, 31, &cnt ) == 0 );
  assert ( CountWorkDays ( 2001,   1,   1,    2000,  1,  1, &cnt ) == 0 );
  assert ( CountWorkDays ( 2001,   1,   1,    2015,  2, 29, &cnt ) == 0 );
  assert ( CountWorkDays ( 2000,  12,   2,    2000, 12,  3, &cnt ) == 1 && cnt ==          0 );
  assert ( CountWorkDays ( 2000,  12,   3,    2000, 12,  3, &cnt ) == 1 && cnt ==          0 );
  assert ( CountWorkDays ( 2016,  11,  17,    2016, 11, 17, &cnt ) == 1 && cnt ==          0 );
  assert ( CountWorkDays ( 2000,  12,   1,    2000, 12,  3, &cnt ) == 1 && cnt ==          1 );
  assert ( CountWorkDays ( 2016,  11,   1,    2016, 11,  1, &cnt ) == 1 && cnt ==          1 );
  assert ( CountWorkDays ( 2000,  11,  30,    2000, 12,  3, &cnt ) == 1 && cnt ==          2 );
  assert ( CountWorkDays ( 2000,  11,  29,    2000, 12,  3, &cnt ) == 1 && cnt ==          3 );
  assert ( CountWorkDays ( 2000,  11,  28,    2000, 12,  3, &cnt ) == 1 && cnt ==          4 );
  assert ( CountWorkDays ( 2000,  11,  27,    2000, 12,  3, &cnt ) == 1 && cnt ==          5 );
  assert ( CountWorkDays ( 2004,  12,  26,    2004, 12, 31, &cnt ) == 1 && cnt ==          5 );
  assert ( CountWorkDays ( 2004,  12,  25,    2004, 12, 31, &cnt ) == 1 && cnt ==          5 );
  assert ( CountWorkDays ( 2016,  11,   1,    2016, 11, 17, &cnt ) == 1 && cnt ==         12 );
  assert ( CountWorkDays ( 2016,  11,   1,    2016, 11, 30, &cnt ) == 1 && cnt ==         21 );
  assert ( CountWorkDays ( 2008,   9,  30,    2008, 11, 11, &cnt ) == 1 && cnt ==         30 );
  assert ( CountWorkDays ( 2000,   5,   8,    2000, 12, 31, &cnt ) == 1 && cnt ==        163 );
  assert ( CountWorkDays ( 2015,   1,   1,    2015, 12, 31, &cnt ) == 1 && cnt ==        252 );
  assert ( CountWorkDays ( 2016,   1,   1,    2016, 12, 31, &cnt ) == 1 && cnt ==        254 );
  assert ( CountWorkDays ( 2014,   3,  27,    2016, 11, 12, &cnt ) == 1 && cnt ==        666 );
  assert ( CountWorkDays ( 2001,   2,   3,    2016,  7, 18, &cnt ) == 1 && cnt ==       3911 );
  assert ( CountWorkDays ( 2000,   1,   1,    2016, 12, 31, &cnt ) == 1 && cnt ==       4302 );
 assert ( CountWorkDays ( 2416,   4,   3,    2531,  1,  4, &cnt ) == 1 && cnt ==      29038 );
 // assert ( CountWorkDays ( 2076,   7,   4, 5094763,  6,  3, &cnt ) == 1 && cnt == 1288603482 );
  assert (CountWorkDays ( 2000, 1, 1, 2000, 1, 33, &cnt ) == 0);
//  assert (CountWorkDays ( 2481, 10, 23, 5063436, 10, 17, &cnt ) == 1 && cnt == 1280574350);
//  assert (CountWorkDays ( 2165, 11, 12, 5079507, 5, 18, &cnt ) == 1 && cnt == 1284720613);

  return 0;
}
#endif /* __PROGTEST__ */
