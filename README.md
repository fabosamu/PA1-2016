# Programming and Algorithmics 1

This is summarum of my first projects from school. Most of them are not that hard, but i want them to have here anyways.
I commented mostly nothing, so the projects are not well documented. I was only learning "how to basic C".

## Names of the projects and rating by school.

Name          	 		| Rating  |
----------------------- | ------- |
01 Plachty	         		| 3.21/5  |
02 SETI                		| 5.50/5  |
03 Pracovní dny			| 6.88/5  |
04 Investice do pozemků	| 7.26/5  |
05 Dálnice		    		| 3.00/3  |
06 CZ sort		     		| 3.00/3  |
08 Pořadová cvičení   		| 3.00/3  |

