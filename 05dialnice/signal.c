#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printData(int * array, int * n){
	int i;
	//printf("%d\n",*n);
	for ( i = 0 ; i < *n ; i++ ){
		printf("%d ",array[i]);
	}
	printf("\n");
}
		
int checkLoad(int from, int to, int ** dataConstruct,int * conSize){
	int i, min;
	//printData(*dataConstruct, conSize);
	//printf("from:%d to:%d cSize:%d\n", from, to, *conSize);
	if ( from > to ||
		to >= *conSize ||
		from < 0 )
		return 0;
	min = (*dataConstruct)[from];
	for ( i = from ; i <= to ; i++ ){
		//printf("ai:%d ; nr:%d\n",(*dataConstruct)[i], i);
		if ( (*dataConstruct)[i] < min )
			min = (*dataConstruct)[i];
	}
	printf("Maximalni naklad: %d\n",min);
	return 1;
}

int setLimits(int ** dataConstruct, int * conSize, int ** dataLimits, int * limSize, int * startPoint){
	int num;
	char ch[2];
	if ( scanf("%1s",ch) != 1){
		return 0;
	}
	if ( strcmp(ch,"[") != 0)
		return 0;
	while (1){
		if ( scanf("%d", &num) != 1)
			return 0;
		if ( num < 1 )
			return 0;
		if ( *startPoint >= *conSize )
			return 0;
		(*dataConstruct)[*startPoint]=num;
		//printf("prepis dat%d i:%d, na %d",(*dataConstruct)[*startPoint],*startPoint,num);
		(*startPoint)++;
		
		if ( scanf("%1s",ch) != 1)
			return 0;
		if ( strcmp(ch,",") != 0 ){
			//printf("zla ciarka");
			if ( strcmp(ch,"]") == 0 ){
				//printf("EOL\n");
				//printData((*dataConstruct), conSize);
				return 1;
			}
			else {
				return 0;
			}
		}
	}
	/*
	if ( *startPoint < 0)
		return 0;
	if ( ( (*startPoint)+(*limSize) ) > (*conSize) )
		return 0;
	for ( i = 0 ; i < (*limSize) ; i++ ){
		(*dataConstruct)[*startPoint] = (*dataLimits)[i];
		(*startPoint)++;
	}
	return 1;
	*/
}

int readData(int ** data, int * n, int * max){
	int num;
	char ch[2];
	if ( scanf("%1s",ch) != 1){
		return 0;
	}
	if ( strcmp(ch,"[") == 0){
		while (1){
			if ( scanf("%d", &num) != 1)
				return 0;
			if ( num < 1 )
				return 0;
			if ( *n >= *max ){
				*max += ( *max < 100 ) 
					? 10
					: *max / 2;
				*data = (int *) realloc ( *data, *max * sizeof ( **data ) );
			}
			(*data)[ *n ] = num;
			(*n)++;
			if ( scanf("%1s",ch) != 1)
				return 0;
			if ( strcmp(ch,",") != 0 ){
				if ( strcmp(ch,"]") == 0 ){
					return 1;
				}
				else {
					return 0;
				}
			}
		}
		
		
	} else	
		return 0;
	
}
		
int readLine(int *cnt, int ** dataConstruct, int * conSize, int * conMax, int ** dataLimits, int * limSize, int * limMax){
	int res, limit, loadA, loadB;
	char command[9];
	res = scanf("%9s\n",command);
	//printf("%s\n",command);
	if (res == EOF){
		//printf("EOF\n");
		return 2;
	} else if (res != 1){
		//printf("zly command"); 
		return 0;
		
	} else if ( strcmp(command,"construct") == 0 ){
		if ( readData(dataConstruct, conSize, conMax) == 0){
			return 0;
		}
		return 1;
		//printData((*dataConstruct),conSize);
		
	} else if ( strcmp(command,"limits") == 0 ){
		if ( scanf("%d", &limit) != 1)
			return 0;
		if ( limit < 0 || limit > *conSize )
			return 0;
		
		/*
		if ( readData(dataLimits, limSize, limMax) == 0){
			//printf("zla limita");
			return 0;
		}
		*/
		if ( ! setLimits(dataConstruct, conSize, dataLimits, limSize, &limit) )
			return 0;
		
	} else if ( strcmp(command,"load") == 0 ){
		if ( scanf("%d", &loadA) != 1){
			return 0;
		}
		if ( scanf("%d", &loadB) != 1){
			return 0;
		}
		if ( ! checkLoad(loadA,loadB,dataConstruct,conSize) )
			return 0;
	} else {
		return 0;
	}
	return 1;
}



int main(){
	int * dataConstruct=NULL;
	int * dataLimits=NULL;
	int cnt=0, conSize=0, conMax=0, limSize=0, limMax=0, res;
	//char ch[1];
	while (1){
		res = readLine(&cnt, &dataConstruct, &conSize, &conMax, &dataLimits, &limSize, &limMax);
		//printf("%d\n", dataConstruct[0]);
		//printf("res=%d\n",res);
		cnt++;
		if (cnt == 1)
			printf("Prikazy:\n");
		if ( ! res ){
			printf("Nespravny vstup.\n");
			free(dataConstruct);
			free(dataLimits);
			return 1;
		}
		if  ( res == 2 )
			break; /*EOF*/
	//printf("conSize: %d\n",conSize);
	}
	//printData(dataConstruct, &conSize);
	//printData(dataLimits, &limSize);
	free(dataConstruct);
	free(dataLimits);
	return 0;
}