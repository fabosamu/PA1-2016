#!/bin/bash
gcc -g -Wall -pedantic signal.c && \
( \
if [ $# -eq 0 ] ; then
	FIL="test.txt"
	read -p "V-algrind or R-un? " -n 1 -r
	echo    # (optional) move to a new line
	if [[ $REPLY =~ ^[Vv]$ ]]
	then
		valgrind -v --track-origins=yes ./a.out <"$FIL"
	fi
	if [[ $REPLY =~ ^[Rr]$ ]]
	then
		./a.out <"$FIL"
	fi
else 
	for i in {0..9}; do
		echo "*****$i*****"
		./a.out < 000"$i"_in.txt > soubor
		paste  soubor 000"$i"_out.txt
		echo
	done
	for i in {10..10}; do
			echo "*****$i*****"
		./a.out < 00"$i"_in.txt > soubor
		paste  soubor 00"$i"_out.txt
		echo
	done

fi
) 
