#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<locale.h>

int main(){
	char ** data=NULL;
	int dataMax=0, dataCnt=0, i;
	
	char * line=NULL;
	char ch;
	int cnt=0, maxLine=0, eof=0;
	
	
	while(1){
		if ( dataCnt >= dataMax ){
			dataMax += ( dataMax < 100 ) ? 10 : dataMax / 2;
			data = (char **) realloc ( data, dataMax * sizeof ( **data) );
		}
		
		
		while(1){
			ch = getchar();
			if ( ch == EOF ){
				eof=1;
				break;
			}
			if ( ch == '\n' ){
				line[cnt]='\0';
				cnt++;
				break;
			}
			
			if ( cnt >= maxLine ){
				maxLine += ( maxLine < 100 ) ? 10 : maxLine / 2;
				line = (char *) realloc ( line, maxLine * sizeof ( *line ) );
			}
			
			if ( ( ch >= 'a' && ch <= 'z' ) || ( ch >= 'A' && ch <= 'Z' ) || ( ch == ' ' ) ){
				line[cnt]=ch;
				cnt++;
			} else {
				printf("Nespravny vstup.\n");
				break;
			}
		}
		if ( eof == 1 ){
			break;
		}
		data[cnt] = (char*)malloc( cnt * sizeof(char) );
		strcpy(data[cnt],line);
		cnt=0;
		maxLine=0;
		printf("%s\n",line);
		
		free(line);
		line = NULL;
		dataCnt++;
	}
	
	
	free(line);
	for ( i = 0 ; i <= dataCnt ; i++ ){
		free(data[i]);
	}
	free(data);
	
	if( eof == 0 ){
		return 1;
	}
}

/*setlocale(LCCOLLATE,"cs_CZ.utf8");*/