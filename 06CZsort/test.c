#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct TItem{
	char phone[21];
	char * name;
	char * crypt;
} TITEM;

int readPhone(char number[]){
	char ch;
	int cnt=0;
	
	ch = getchar();
	if (ch != ' '){
		while(1){
			if ( ch == '\n' ){
				return 0;
			}
			ch = getchar();
		}
		return 0;
	}
	
	while (1){
		if ( cnt == 19 ){
			printf("memExceedERR");
			while(1){
				ch = getchar();
				if ( ch == '\n' ){
					break;
				}
			}	
			return 0;
		}
		ch = getchar();
		if (ch == ' '){
			printf("space~pocetNum%d\n",cnt);
			number[cnt] = '\0';
			return 1;
		} else if ( ( ch < '0' || ch > '9' ) ){
			printf("badInterval\n");
			while(1){
				ch = getchar();
				if ( ch == '\n' ){
					return 0;
				}
			}	
			return 0;
		} else {
			number[cnt] = ch;
			printf("%c", number[cnt]);
			cnt++;
		}
	}
}

int readName(char ** name, int * size){
	char ch, chZ;
	int cnt=0, max=0;
	char * string=NULL;
	
	chZ = getchar();
	if ( ( chZ < 'A' || chZ > 'Z' ) && ( chZ < 'a' || chZ > 'z' ) ){/*prvy znak v retazci*/
		printf("intervalCharERR,ch:%c\n",chZ);
		while(1){
			ch = getchar();
			if ( ch == '\n' ){
				return 0;
			}
		}
		return 0;
	}
	//printf("%c->%d\n",chZ,cnt);
	cnt++;
	while(1){
		ch = getchar();
		//printf("%d->%d\n",ch, cnt);
		
		if ( cnt >= max ){
			max += ( max < 100 ) ? 10 : max / 2;
			string = (char *) realloc ( string, max * sizeof ( *string ) );
		}
		
		if ( ch == '\n' ){/*je koniec riadku, posli to cele von z cyklu*/
			printf("EOL;%d",cnt);
			if ( string[cnt-1] == ' ' ){
				return 0;
			}
			string[cnt]='\0';
			break;
		}
		if ( ! ( ( ch >= 'a' && ch <= 'z' ) || ( ch >= 'A' && ch <= 'Z' ) || ( ch == ' ' ) ) ){/*ostatne znaky v retazci*/
			printf("intervalCharERR,ch:%c\n",ch);
			//printf("P:%d\n", &string);
			while(1){
				ch = getchar();
				printf("%c", ch);
				if ( ch == '\n' ){
					break;
				}
			}
			if ( string != NULL ){
				free(string);
			}
			return 0;
		}
		
		string[cnt]=ch;
		cnt++;
	}
	string[0]=chZ;
	/*printf("%c",string[0]);
	printf("%d",string[cnt]);
	printf("endOfWhile\n");
	printf("%s", string);
	for (int i=0 ; i<=cnt ; i++){
		printf("%d,", string[i]);
	}
	printf("Realloced");*/
	//*name = ( char * ) realloc ( *name, ( cnt ) * sizeof ( *name ) ); 
	*name = (char*)malloc( cnt * sizeof ( *name ) );
	/********************************** BACHA!!! SPRAVNY REALLOC? ************/
	//printf("size:%d", cnt);
	//strncpy(*name,string,sizeof(*string));
	*name = (char*)malloc( cnt * sizeof(char) );
	*size = cnt;
	free(string);
	printf("OutOfReadName\n");
	return 1;
	
}

int readLine(TITEM * line, int dataCount){
	char ch;
	char number[21];
	char * name=NULL;
	int nameSize;
	
	while(1){
		ch = getchar();
		if (ch == '+'){
			if ( ! readPhone(number) ){
				return 0;
			}
			printf("HaveReadPhone\n");
			printf("%s\n",number);
			
			if ( ! readName(&name, &nameSize) ){
				free(name);
				return 0;
			}
			printf("HaveReadName\n");
			/*pridavam do struktury LINE*/
			//printf("%s ; %d", name, nameSize);
			//strcpy( line -> phone, number);
			
			//strncpy( line -> name, name, nameSize );
			
			//printf("strcpyNameOK\n");
			/*uvolnujem pomocnu premennu, nastav na NULL*/
			free(name);
			name=NULL;
			printf("OK\n");
			/*pridat do pola struktur, overit duplicitu*/
			return 1;
		} else if (ch == '?'){
			
		} else if (ch == EOF){
			return EOF;
		} else {
			while(1){
				ch = getchar();
				if ( ch == '\n' ){
					break;
				}
			}
			return 0;
		}
	}
	return 1;
}

int main(){
	//TITEM * data=NULL;
	TITEM line;
	int res, dataCount=0;
	
	while(1){
		res = readLine(&line, dataCount);
		if (res == EOF){
			printf("EOF\n");
			break;
		} else if (res == 0){
			printf("Nespravny vstup.\n");
		} else {
			printf("HaveReadLine\n");
			//printf("LALALALALALLALALALAALL%s", line . phone );
			dataCount++;
		}
	}
	
	return 0;
}