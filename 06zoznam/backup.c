#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*
typedef struct TSizeMax{
	int nameSize;
	int phoneSize;
	int cryptSize;
} TSIZEMAX;
*/

typedef struct TItem{
	char phone[21];
	char * name;
	char * crypt;
	int nameSize;
} TITEM;


/******************************************************** PHONE ***********************************/
int readPhone( char * phone ){
	char ch;
	int n=0;
	ch = getchar();
	if ( ch != ' ' ){
		//printf("spaceERR\n");
		return 0;
	}
	while( 1 ){
		ch = getchar();
		//printf("%c, ",ch);
		if ( ch == ' ' || ch == '\n' ){
			//printf("\\n\n");
			phone[n] = '\0';
			return 1;
		}
		if ( n > 19 ){
			ch = getchar();
			if ( ch != ' ' ){
				return 0;
			}
			phone[n] = '\0';
			break;
		}
		if ( ! ( ch >= '0' && ch <= '9' ) ){
			//printf("numberERR\n");
			return 0;
		}
		phone[n] = ch;
		n++;
	}
	return 1;
}

char cryptChar(char ch){
	switch (ch){
		case 'A' :
		case 'a' :
		case 'B' :
		case 'b' :
		case 'C' :
		case 'c' :
			return '2';
		case 'D' :
		case 'd' :
		case 'E' :
		case 'e' :
		case 'F' :
		case 'f' :
			return '3';
		case 'G' :
		case 'g' :
		case 'H' :
		case 'h' :
		case 'I' :
		case 'i' :
			return '4';
		case 'J' :
		case 'j' :
		case 'K' :
		case 'k' :
		case 'L' :
		case 'l' :
			return '5';
		case 'M' :
		case 'm' :
		case 'N' :
		case 'n' :
		case 'O' :
		case 'o' :
			return '6';
		case 'P' :
		case 'p' :
		case 'Q' :
		case 'q' :
		case 'R' :
		case 'r' :
		case 'S' :
		case 's' :
			return '7';
		case 'T' :
		case 't' :
		case 'U' :
		case 'u' :
		case 'V' :
		case 'v' :
			return '8';
		case 'W' :
		case 'w' :
		case 'X' :
		case 'x' :
		case 'Y' :
		case 'y' :
		case 'Z' :
		case 'z' :
			return '9';
		case ' ' :
			return '1';
		default :
			return '0';
	}
}

/******************************************************** read NAME ***********************************/
int readName( char ** name, int * nameSize, char ** cryptName){
	char * string=NULL;
	char * crypt=NULL;
	int ch;
	int max=0,size=0;
	//printf("nameIN\n");
	while ( 1 ){
		ch = getchar();
		if ( size >= max ){
			max += ( max < 100 ) ? 10 : max / 2;
			//printf("realloc, max: %d size: %d", max, size);
			string = (char *) realloc ( string, max * sizeof ( *string ) );
			crypt = (char *) realloc ( crypt, max * sizeof ( *crypt ) );
		}
		if ( size == 0 ){
			if ( ! ( ( ch >= 'a' && ch <= 'z' ) || ( ch >= 'A' && ch <= 'Z' ) ) ){/*NEpatri do intervalov*/
				printf("~ %d ~intervalERR\n", ch);
				free(string);
				free(crypt);
				return 0;
			}
		}
		if ( ch == '\n' ){
			ch='\0';
			string[size]='\0';
			crypt[size]='\0';
			*name = (char *)malloc(size+1);
			*cryptName = (char*)malloc(size+1);
			strncpy(*name, string, size+1);
			strncpy(*cryptName, crypt, size+1);
			*nameSize = size+1;
			free(string);
			free(crypt);
			return 1;
		}
		if ( ! ( ( ch >= 'a' && ch <= 'z' ) || 
			   ( ch >= 'A' && ch <= 'Z' ) ||
			   ( ch == ' ') ) ) { /*NEpatri do intervalov alebo to nie je medzera*/
				   if ( ch == EOF ){
					   return 2;
				   }
				printf("interval2ndERR~ %c ~BAD\n",ch);
				free(string);
				free(crypt);
				return 0;
		}
		string[size]=ch;
		crypt[size]=cryptChar(ch);
		printf("%c, ", string[size]);
		size++;
	}
	
}

int checkDuplicity(TITEM ** field, char ** name, char phone[], int fieldSize){
	int i=0;
	printf("lineNo: %d", fieldSize);
	printf("CheckDuplicity, %s vs %s & %s vs %s\n",field[i] -> name, *name, field[i] -> phone, phone);
	for ( i = 0 ; i < fieldSize ; i++ ){
		if ( ( strcmp(field[i] -> name, *name) == 0 ) && ( ! strcmp(field[i] -> phone, phone) == 0 ) ){
			return 1;
		}
	}
	return 0;
}/***********************************************************************************************************dorob checkDuplicity, spravne pointery*/

/******************************************************** LINE ***********************************/
int readLine ( TITEM * data, TITEM ** dataField, int dataFieldSize ){
	/*EOF=2 ; chyba = 0 ; spravne 1*/
	char ch;
	char phoneNum[21];
	char * name;
	char * crypt;
	int nameSize=0;
	
	ch = getchar();
	if ( ch == '+' ){
		if ( ! readPhone( phoneNum ) ){
			printf("readLineNumERR\n");
			return 0;
		}
		strncpy( data -> phone, phoneNum, strlen(phoneNum) );
		if ( ! readName( &name, &nameSize, &crypt ) ){
			printf("readLineNameERR\n");
			return 0;
		}
		
		if ( dataFieldSize != 0 ){
			/*
			if ( checkDuplicity(dataField,&name,phoneNum, dataFieldSize) ){
				printf("isdup\n");
				//return 3;
			}
			*/
			//printf("CheckDuplicity, %s ~vs~ %s & %s vs %s\n",dataField[dataFieldSize] -> name, name, dataField[dataFieldSize] -> phone, phoneNum);
		}
		
		
		data -> name = (char *)malloc(nameSize);
		data -> crypt = (char *)malloc(nameSize);
		strncpy( data -> name, name, nameSize );
		strncpy( data -> crypt, crypt, nameSize );
		free(name);
		free(crypt);
		//printf("%s",phoneName);
		return 1;
	} else if ( ch == '?' ){
		//getPhone();
		return 1;
	} else if ( ch == '\n' ){
		//printf("newlineERR");
		return 0;
	}else if ( ch == EOF ){
		return 2;
	} else {
		while(1){
			ch=getchar();
			if ( ch == '\n' ){
				return 0;
			}
		}
	}
}

/******************************************************** MAIN ***********************************/
int main(){
	TITEM * data=NULL;
	int res, maxData=0, sizeData=0, i;
	
	while (1){
		if ( sizeData >= maxData ){
			maxData += ( maxData < 100 ) ? 10 : maxData / 2;
			data = (TITEM *) realloc ( data, maxData * sizeof ( *data ) );
			printf("sizeData: %d, maxData: %d",sizeData, maxData);
		}
		res = readLine ( &data[sizeData], &data, sizeData );
		printf("%s\n", data[sizeData] . phone);
		if ( res == 2 ){/*EOF*/
			break;
		} else if ( res == 1 ){
			printf("OK\n");
			sizeData++;
			continue;
		} else if ( res == 0 ){
			printf("Nespravny vstup.\n");
			continue;
		} else if ( res == 3 ){
			printf("Kontakt jiz existuje.\n");
		}
	}
	
	for ( i = 0 ; i < sizeData ; i++ ){
		//printf("%s",data[i].name);/*SKONTROLUJ VYPISY, CI SI SPRAVNE NACITAL. DOROB CRYPT ; DUPLICITY ; FIND...*/
		//printf("%s~%d",data[i] . name, i );
		free(data[i] . name);
		free(data[i] . crypt);
	}
	free(data);
	return 0;
}
